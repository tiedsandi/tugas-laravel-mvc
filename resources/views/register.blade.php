<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <h1>Bat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label for="First">First name:</label> <br>
        <input for="First" name="awal"  type="text"><br>
        <label for="Last" >Last name:</label><br>
        <input for="Last" name="akhir" type="text"><br><br>
        <Label>Gender :</Label><br>
        <input type="radio" name="gender"> Male<br>
        <input type="radio" name="gender"> Female<br>
        <input type="radio" name="gender"> Other<br><br>
        <Label>Nationality:</Label>
        <select> 
            <option>Indonesia</option>
            <option>Singapura</option>
            <option>Malaysia</option>
            <option>Australia</option>
        </select> <br><br>
        <Label>Language Spoken:</Label> <br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other
        <br><br>
        <Label>Bio:</Label><br>
        <textarea cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="sign up">
    </form>
</body>
</html>